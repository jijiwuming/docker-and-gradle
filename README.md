# docker-and-gradle

## info

* this is a dockerfile for building a docker container with docker and gradle, design to build and deploy myself project

## how to use the image

* It show as [jijiwuming/docker-and-gradle](https://hub.docker.com/r/jijiwuming/docker-and-gradle/)

```Dockerfile

FROM jijiwuming/docker-and-gradle:latest

```